# Hackathon_Rust
## Description
A client-server program written in Rust supporting remote execution of three 
commands: Shell, File IO, and Control.
## Building
```
cargo build
```
## Running
### General usage
```
cargo run --bin <program name> <args>
```
### Server usage
```
cargo run --bin server <listening port>
```
### Client usage
```
cargo run --bin client <server ip address> <server port>
```
### Try running this
Run above server and client, then on server do:
```
get <file name> <save file as>
shell
ls
```
Will show that you successfully read file from client onto server and saved
it as whatever you called it.
## Wire Protocol Format
```
   0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 25 27 28 29 30 31
   --------------------------------------------------------------------------------------
0  |      magic   |                      length                 |   checksum             |
   --------------------------------------------------------------------------------------
32 |  command     |       subcommand     |                extra data                     |
   --------------------------------------------------------------------------------------
64 |                                     data                                            |
   --------------------------------------------------------------------------------------
128|                                     data                                            |
   --------------------------------------------------------------------------------------
```
## Wire Protocol Fields:
### Magic Byte - 0xFA
### Length - total length of packet
### CMD:
    0 - Shell
    1 - File IO
    2 - Control
### Subcommand:
####    Shell:
        0 - read
        1 - write
        2 - open
        3 - close
####    File IO:
        0 - Start file
        1 - Stop file
        2 - File data
####    Control:
        0 - Close

## Cool Diagram
```
Client                    Server                 You

      __                   ___
     |  |                 |   |                  +  +
     /  /                 |   |                 -    -
    ----                  |   |                  ----
                          -----               
```
