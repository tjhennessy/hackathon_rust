use std::error::Error;
use std::fs::File;
use std::path::Path;
use std::io::*;

#[derive(Debug)]
pub struct FileCtx 
{
	file_hndl: File,
	length: i32,
	/* Other fields go here... */	
}

// This should be good to go now
impl Read for FileCtx {
	fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
		// Each call to read() attempts to pull bytes from
			// this src into a provided buffer
		//buf = self.file_hndl.read();
		self.file_hndl.read(buf)
		// Result<usize> is std::io::Result
	}
}

// This needs work
impl Write for FileCtx {
	fn write(&mut self, buf: &[u8]) -> Result<usize> {
		self.file_hndl.write(buf)
	}

	/* Flush output stream, ensuring all intermediately
		buffered contents reach their destination */
	// Do not think flush needs to return anything here
	fn flush(&mut self) -> Result<()> {
		self.file_hndl.flush()
	}

}


impl FileCtx {
	pub fn from_filename(filename: &String) -> FileCtx {
	
		let path = Path::new(filename);
		let display = path.display();

		// Open the path in read-only mode, returns `io::Result<File>`
		let file = match File::open(&path)
		{
			/* The `description` method of `io::Error` returns a
			string that describes the error */
			Err(why) => panic!("couldn't open {}: {}", display,
				why.description()),
			Ok(file) => file,
		};

		/* Read the file contents into a string, 
			returns `io:Result<usize>` */
		//let mut s = String::new();
		//match file.read_to_string(&mut s)
		//{
		//	Err(why) => panic!("couldn't read {}: {}", display,
		//		why.description()),
		//	Ok(_) => print!("{} contains:\n{}", display, s),
		//}

		// Create a Context struct
		//let ctx = FileCtx { file_hndl: file, length: s.len() as i32 };


		FileCtx { 
			file_hndl: file, 
			length: 0,
			}
	}
}

/* Driver to test functions... This is a library, so main() won't 
	normally be called */
/*
fn main() 
{
	/* Create a context from a file path */
	let mut ctx = FileCtx::from_filename(&"hello.txt".to_string());
  println!("Our new ctx: {:?}", ctx);
	
	/* Write to the file */
	let new_text : String = "this is more data".to_owned();
	ctx.write(&new_text.as_bytes()).unwrap();

	/* Read the file into a buffer */
	let mut buffer = [0; 64];
	match ctx.read(&mut buffer)
	{
		Err(_) => panic!("read() from ctx failed"),
		Ok(_) => println!("read() from ctx succeded"),
	}
	let s = String::from_utf8_lossy(&buffer);
	println!("Result of read() = {}", s);
}
*/