use std::io::prelude::*;
use std::process::{Child, Command, Stdio};


#[derive(Debug)]
pub struct Shell {
    pid: u32,
    pub shell: Child,
}

impl Shell {
    pub fn spawn() -> Result<Shell, String> {
        let shell_res = Command::new("bash")
                                .stdin(Stdio::piped())
                                .stdout(Stdio::piped())
                                .spawn();

        if ! shell_res.is_ok()
        {
            return Err("Unable to do a thing".to_string());
        }

        let shell_opt = shell_res.ok();

        if shell_opt.is_none()
        {
            return Err("shell was none".to_string());
        }

        let shell = shell_opt.unwrap();

        Ok( Shell {
            pid: shell.id(),
            shell: shell,
        })
    }

    pub fn cmd_write(&mut self, cmd:String) -> Result<String, String> {
        let cmd_write = self.shell.stdin.as_mut().unwrap().write_all(cmd.as_bytes()); 

        if ! cmd_write.is_ok()
        {
            return Err("Unable to do a thing".to_string());
        }
        else
        {
            return Ok("Did a thing".to_string());
        }
    }

    pub fn cmd_read(&mut self) -> Result<Vec<u8>, String> {
        let mut buffer = [0; 128];
        
        let cmd_read = match self.shell.stdout.as_mut().unwrap().read(&mut buffer){
            Ok(size) => size,
            Err(_) => return Err("Unable to to read".to_owned()),
        };

        let data_vec = buffer[..cmd_read as usize].to_vec();

        Ok(data_vec)        
    }
}





/*
fn main() {

    let word = "ifconfig\n".to_string();

    let mut test = Shell::spawn().unwrap();

    let test2 = test.cmd_write(word).unwrap();
    println!("{}", test2);

    println!("Look what comes back");

    let test3 = test.cmd_read().unwrap();

    for _i in 0..128
        {
            print!("{}", test3[_i] as char);
        }
    println!();
    println!("PID: {}", test.pid);

}
*/

/*
fn run_cmd(cmd:String, args:String)
{
    let process = match Command::new(&cmd)
                                    .stdin(Stdio::piped())
                                    .stdout(Stdio::piped())
                                    .spawn() {
            Err(why) => panic!("couldn't spawn {}: {}", cmd, why.description()),
            Ok(process) => process,
        };

    match process.stdin.unwrap().write_all(args.as_bytes()) {
        Err(why) => panic!("couldn't write to wc stdin: {}",
                           why.description()),
        Ok(_) => println!("sent args to {}", cmd),
    }

    let mut s = String::new();
    match process.stdout.unwrap().read_to_string(&mut s) {
        Err(why) => panic!("couldn't read wc stdout: {}",
                           why.description()),
        Ok(_) => print!("{} responded with:\n{}", cmd, s),
    }
}

fn main() {
    run_cmd("bash".to_string(), "netstat -auntp".to_string())
}
*/