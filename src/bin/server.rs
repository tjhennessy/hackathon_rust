
extern crate wire_proto;
extern crate mio;
extern crate shlex;

use std::env;
use mio::*;
use mio::net::{TcpStream, TcpListener};
use mio::unix::EventedFd;
use std::net::SocketAddr;
use std::net::Shutdown;
use std::fs::File;
use std::io::{Read,Write,self,BufRead};
use wire_proto::{Message, Cmd, framing, Extra};

const STDIN_TOK : Token = Token(0);
const LISTEN_TOK : Token = Token(1);
const CLIENT_TOK : Token = Token(2);


struct FileTrans {
    pub src: String,
    pub dest: String,
    pub file: File,
    pub start: std::time::Instant,
    pub size: usize,
}

struct Server {
    poll : Poll,
    serv_sock: TcpListener,
    client_sock: Option<TcpStream>,
    file_transfer: Option<FileTrans>,
    should_run : bool,
    shell_mode : bool,
}

impl Server {
    fn new(port: &String) -> Server {

        let mut addr_str = "0.0.0.0:".to_owned();
        addr_str.push_str(port);
        let addr:SocketAddr = addr_str.parse().unwrap();

        Server {
            poll: Poll::new().unwrap(),
            serv_sock: TcpListener::bind(&addr).unwrap(),
            client_sock: None,
            file_transfer: None,
            should_run: true,
            shell_mode: false,
        }
    }

    pub fn run(&mut self) {

        self.poll.register(&self.serv_sock, LISTEN_TOK, 
                Ready::readable(), PollOpt::level()).unwrap();

        let fd0_e = EventedFd(&0);

        self.poll.register(&fd0_e, STDIN_TOK, Ready::readable(), PollOpt::level()).unwrap();

        let mut events = Events::with_capacity(1024);

        let mut framing = framing::Framing::new();

        let mut buf = [0u8; 128];

        self.print_prompt();
        
        'main_loop: while self.should_run { 

            self.poll.poll(&mut events, None).unwrap();

            'events: for event in events.iter() {
                match event.token() {
                    STDIN_TOK => {
                        if self.shell_mode {
                            self.handle_shell();
                        }
                        else
                        {
                            self.handle_command();
                        }
                        self.print_prompt();
                    }
                    LISTEN_TOK => {
                        let (new_conn,addr) = self.serv_sock.accept().unwrap();
                        if self.client_sock.is_some() {
                            new_conn.shutdown(Shutdown::Both).unwrap();
                            continue 'events;
                        }
                        else
                        {
                            println!("New Connection from {}.", addr);
                            self.client_sock = Some(new_conn);
                            self.poll.register(self.client_sock.as_ref().unwrap(), CLIENT_TOK, 
                                Ready::readable(), PollOpt::level()).unwrap();
                        }
                    }
                    CLIENT_TOK => {
                        let size = self.client_sock.as_ref().unwrap().read(&mut buf).unwrap();
                        if size == 0
                        {
                            break 'main_loop;
                        }
                        framing.add_data(&buf[..size]);
                        while framing.msg_ready() {
                            let msg_opt = framing.get_message();
                            if msg_opt.is_some() {
                                let msg = msg_opt.unwrap();
                                match msg.cmd {
                                    Cmd::Shell => {
                                        if self.shell_mode {
                                            let _ = io::stdout().lock().write(&msg.data).unwrap();
                                        }
                                    },

                                    Cmd::FileIO => {
                                        if self.file_transfer.is_some() {
                                            self.handle_file_data(&msg);
                                        }
                                    },

                                    _ => {}
                                }
                            }
                        }
                    }

                    _ => {}
                }
            }

        }
    }

    fn handle_command(&mut self) {
        let line = io::stdin().lock().lines().next().unwrap().unwrap();
        let tokens = shlex::split(&line).unwrap();

        if tokens.len() == 0 {
            return;
        }

        match tokens[0].as_ref() {
            "exit" => self.should_run = false,
            "dissconnect" => {
                self.poll.deregister(self.client_sock.as_ref().unwrap()).unwrap();
                self.client_sock = None;
            }
            "shell" => {
                if self.client_sock.is_none() {
                    println!("No client connected");
                }
                else
                {
                    self.shell_mode = true;
                }
            }
            "get" => {
                if self.client_sock.is_none()
                {
                    println!("No client connectd");
                }
                else {
                    if self.file_transfer.is_some() {
                        println!("File transfer already in progress.");
                    }
                    else
                    {
                        self.file_transfer = Some(
                            FileTrans {
                                src: tokens[1].clone(),
                                dest: tokens[2].clone(),
                                file: File::create(tokens[2].clone()).unwrap(),
                                start: std::time::Instant::now(),
                                size: 0,
                            }
                        );

                        let msg_data = Message::build_message(Cmd::FileIO, 0, Extra::Full(0), tokens[1].clone().as_bytes().to_vec()).to_data().unwrap();
                        self.client_sock.as_mut().unwrap().write_all(&msg_data).unwrap();
                    }
                }
            }
            _ => println!("Unknown Command"),
        }
    }

    fn handle_shell(&mut self) {
        let mut line = io::stdin().lock().lines().next().unwrap().unwrap();
        
        if line == "exit" {
            self.shell_mode = false;
            return;
        }

        line.push_str("\n");
        let data = Message::build_message(Cmd::Shell, 1, Extra::Full(0), line.as_bytes().to_vec()).to_data().unwrap();
        let _ = self.client_sock.as_mut().unwrap().write_all(&data).unwrap();
    }

    fn print_prompt(&self) {
        if self.shell_mode {
            print!("[]# ");
        }
        else
        {
            print!("~> ");
        }
        io::stdout().flush().unwrap();
    }

    fn handle_file_data(&mut self, msg: &Message) {
        match msg.subcmd {
            1 => {
                {
                    let f = self.file_transfer.as_ref().unwrap();
                    let t = std::time::Instant::now() - f.start;
                    println!("Transfered {} bytes in {} seconds", f.size, t.as_secs());
                }
                self.file_transfer = None; // kill the transfer
            },
            2=> {
                if self.file_transfer.is_none() {
                    println!("Got file data with no transfer active");
                    return;
                }
                else{
                    self.file_transfer.as_mut().unwrap().file.write_all(&msg.data).unwrap();
                    self.file_transfer.as_mut().unwrap().size += msg.data.len();
                }
            }
            _ => {}
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("Usage: {} [listen port]", args[0]);
        return;
    }

    Server::new(&args[1]).run();
}
