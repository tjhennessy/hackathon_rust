use std::option::Option;
use std::mem::transmute; 

use crate::{MAGIC_VALUE, Message};

pub struct Framing {
    framing : bool, //
    data_buf : Vec<u8>, // data buf
    length : u16,
}

impl Framing {
    pub fn new() -> Framing {
        Framing {
            framing : false,
            data_buf : Vec::new(),
            length : 0,
        }
    }

    pub fn add_data(&mut self, data : &[u8]) {
        self.data_buf.extend_from_slice(data);

        if !self.framing {
            let mut index = 0;
            while index < self.data_buf.len() && self.data_buf[index] != MAGIC_VALUE {
                index += 1;
            }

            if index == self.data_buf.len() {
                self.data_buf.clear();
            }
            else{
                assert!(self.data_buf[index] == MAGIC_VALUE);
                if index != 0 {
                    self.data_buf = self.data_buf[index..].to_vec();
                }
                self.framing = true;
            }
        }
    }

    pub fn msg_ready(&mut self) -> bool {
        if self.framing {
            if self.data_buf.len() == 0 {
                self.framing = false;
                return false;
            }
            assert!(self.data_buf[0] == MAGIC_VALUE);
            if self.data_buf.len() < 8 {
                return false;
            }

            let mut len_arr = [0u8; 2];
            len_arr.copy_from_slice(&self.data_buf[1..3]);
            let len = unsafe { u16::from_be(transmute::<[u8; 2], u16>(len_arr)) };
            if len as usize <= self.data_buf.len(){
                let check = self.data_buf[3]; // get the checksum
                self.data_buf[3] = 0; // set message checksum to 0
                let msg_check = Message::get_checksum(&self.data_buf[..len as usize]);
                if check == msg_check {
                    self.data_buf[3] = check; // restore the checksum
                    self.length = len;
                    return true;
                }
                else {
                    self.framing = false;
                    self.add_data(&[]);
                }
            }
        }

        return false;
    }

    pub fn get_message(&mut self) -> Option<Message> {
        if !self.msg_ready() {
            return None;
        }

        let msg_data = self.data_buf[..self.length as usize].to_vec();
        if self.length as usize == self.data_buf.len() {
            self.data_buf.clear();
        }
        else {
            self.data_buf = self.data_buf[self.length as usize..].to_vec();
        }

        self.length = 0;

        let new_message = Message::from_data(&msg_data);

        match new_message {
            Ok(msg) => Some(msg),
            Err(s) => panic!(s),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::{Cmd, Extra};

    #[test]
    fn frame_test() {
        let data = vec![1,2,3];
        let message = Message::build_message(Cmd::Control, 0, Extra::Full(0), data);
        let mut framing = Framing::new();
        let msg_data = message.to_data();
        assert!(msg_data.is_ok());
        framing.add_data(&msg_data.unwrap());
        assert!(framing.msg_ready());
        let new_message = framing.get_message().unwrap();
        assert!(new_message.cmd == Cmd::Control);
        assert!(new_message.subcmd == 0);
        assert!(new_message.data[0] == 1);
        assert!(new_message.data[1] == 2);
        assert!(new_message.data[2] == 3);
    }

    #[test]
    fn frame_test_extra_data() {
        let data = vec![1,2,3];
        let extra_data : Vec<u8> = vec![4,5,6]; 
        let message = Message::build_message(Cmd::Control, 0, Extra::Full(0), data);
        let mut framing = Framing::new();
        let msg_data = message.to_data();
        assert!(msg_data.is_ok());
        framing.add_data(&msg_data.unwrap());
        framing.add_data(&extra_data);
        assert!(framing.msg_ready());
        let new_message = framing.get_message().unwrap();
        assert!(new_message.cmd == Cmd::Control);
        assert!(new_message.subcmd == 0);
        assert!(new_message.data[0] == 1);
        assert!(new_message.data[1] == 2);
        assert!(new_message.data[2] == 3);
    }
}