pub mod framing;

use std::result::Result;
use std::string::String;
use std::mem::transmute;

pub const MAGIC_VALUE: u8 = 0xaf;
pub const HEADER_SIZE: u8 = 8;

#[derive(Copy,Clone,Debug,PartialEq)]
pub enum Cmd {
    Shell,
    FileIO,
    Control,
}

impl Cmd {
    pub fn val(&self) -> u8 {
        match self {
            Cmd::Shell => 0,
            Cmd::FileIO => 1,
            Cmd::Control => 2,
        }
    }

    pub fn from_val(val:u8) -> Cmd {
        match val {
            0 => Cmd::Shell,
            1 => Cmd::FileIO,
            2 => Cmd::Control,

            _ => panic!("Cmd value {} invalid.", val),
        }
    }
}

#[derive(Copy,Clone,Debug)]
pub struct ExtraBytes{
    pub e1 : u8,
    pub e2 : u8,
}

#[derive(Copy,Clone,Debug)]
pub enum Extra {
    Bytes(ExtraBytes),
    Full(u16),
}

#[derive(Clone,Debug)]
pub struct Message {
    pub cmd : Cmd,
    pub subcmd : u8,
    pub extra : Extra,
    pub data : Vec<u8>,
}

impl Message {
    pub fn build_message(cd : Cmd, sbcmd : u8, xtra : Extra, dta : Vec<u8>) -> Message {
        let msg = Message {
            cmd : cd,
            subcmd : sbcmd,
            extra : xtra,
            data : dta,
        };
        msg
    }
    
    pub fn from_data(data : &[u8]) -> Result<Message, String> {
        // Step 1: Strip header bytes
        if MAGIC_VALUE != data[0] {
            return Err("invalid magic byte in data passed to from_data".to_owned());
        }
        let mut len_arr = [0u8; 2];
        len_arr.copy_from_slice(&data[1..3]);
        let packet_len: u16 = u16::from_be(unsafe {transmute::<[u8; 2], u16>(len_arr)});

        if packet_len as usize != data.len() {
            return Err("Invalid data size".to_owned());
        }

        let mut data_cpy = data.to_vec();
        data_cpy[3] = 0;
        if Message::get_checksum(&data_cpy) != data[3] {
            return Err("checksum values do not match in from_data".to_owned());
        }

        // Step 2: Get payload
        // TODO: Take care of endianness for extra and data at some point
        // TODO: compute checksum and compare it 
        let msg = Message::build_message(Cmd::from_val(data[4]), 
                                         data[5], 
                                         Extra::Full(0), 
                                         data[8..data.len()].to_vec());
        
        Ok(msg)
    }

    pub fn to_data(mut self) -> Result<Vec<u8>, String> {
        let mut ret : Vec<u8> = Vec::new();
        // add magic bytes
        ret.push(0xaf);   
        // packet length is valid or it's an error
        let packet_length = match self.get_size(){
            Ok(len) => len,
            Err(e) => return Err(e),
        };
        // set length_bytes as a vector of two bytes and order in big endian
        let length_bytes = unsafe {&transmute::<u16, [u8; 2]>(packet_length.to_be())};
        // add packet length
        ret.extend_from_slice(length_bytes);
        // add checksum as 0 -- will calculate later
        ret.push(0);
        ret.push(self.cmd.val());
        ret.push(self.subcmd);
        // push extra bytes -- fix later
        ret.push(0);
        ret.push(0);

        ret.append(&mut self.data);

        ret[3] = Message::get_checksum(&ret);

        Ok(ret) // return ret
    }

    fn get_size(&self) -> Result<u16, String> {
        let data_size: u16 = self.data.len() as u16;
        // HEADER_SIZE is u8, 
        // packet_size must be u32 to detect overflow case of u16
        let packet_size: u32 = ((HEADER_SIZE as u16) + data_size) as u32;
        if packet_size >= (std::u16::MAX as u32) {
            return Err("packet size larger than wire protocol specification".to_owned());
        }
        Ok(packet_size as u16)
    }

    fn get_checksum(data: &[u8]) -> u8 {
        let mut sum: u8 = 0;
        for i in data {
            sum = sum.wrapping_add(*i);
        }
        sum
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_message_test() {
        let data = vec![1, 2, 3];
        let test_msg = Message::build_message(Cmd::Shell, 3u8, Extra::Full(0), data);
        let msg_data = test_msg.to_data();
        assert!(msg_data.is_ok());
        println!("{:?}", msg_data);
        let new_msg = Message::from_data(&msg_data.unwrap()).unwrap();
        assert!(new_msg.data[0] == 1);
        assert!(new_msg.data[1] == 2);
        assert!(new_msg.data[2] == 3);
        assert!(new_msg.cmd == Cmd::Shell);
        assert!(new_msg.subcmd == 3);
    }
}